position = {
    (1, 1) : {'lat': None, 'lon': None},
    (2, 1) : {'lat': None, 'lon': None}
}

beacons = [
    {'name': 'bc1',
    'lat': 50.067715,
    'lon': 19.990843,
    'owner': None},
    {'name': 'bc2',
    'lat': 50.067773,
    'lon': 19.991168,
    'owner': 1},
    {
    'name': 'bc3',
    'lat': 50.068045,
    'lon': 19.993324,
    'owner': 2
    }
]

def beacon_by_name(beacons, name):
    for i, beacon in enumerate(beacons):
        if beacon['name'] == name:
            return i, beacon

def beacons_by_owner(beacons, owner):
    for i, beacon in enumerate(beacons):
        if beacon['owner'] == owner:
            yield((i, beacon))