from flask import request, jsonify
from flask_restplus import Resource, cors
from ctf_api.restplus import api
from ctf_api.data import position, beacons, beacon_by_name
from ctf_api.serializers import position as position_model

ns = api.namespace('ctf/pos', description='Position operations')

@ns.route('/<int:team>/<int:user>')
class PositionItem(Resource):
    @cors.crossdomain(origin='*')
    @ns.expect(position_model)
    def post(self, team, user):
        # import pdb; pdb.set_trace()
        try:
            post_data = request.json
            print("DATA: {}".format(post_data))
            position[(team, user)]['lat'] = post_data['lat']
            position[(team, user)]['lon'] = post_data['lon']
            for beacon in beacons:
                if beacon['name'].upper() in [beacon_name.upper() for beacon_name in post_data['beacons']]:
                    print('==== updating {} -> {}'.format(beacon['name'], team))
                    beacon['owner'] = team
            return jsonify(post_data), 200
        except KeyError:
            return None, 404
