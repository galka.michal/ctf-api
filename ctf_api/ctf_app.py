import sys
from flask import Flask, Blueprint

from ctf_api.restplus import api
from ctf_api.beacons import ns as beacons_ns
from ctf_api.position import ns as position_ns
from ctf_api.players import ns as players_ns
from ctf_api.database import db


app = Flask(__name__)

def configure_app(flask_app):
    flask_app.config['SERVER_NAME'] = '0.0.0.0:5000'

def initialize_app(flask_app):
    blueprint = Blueprint('api', __name__, url_prefix='/api')
    api.init_app(blueprint)
    api.add_namespace(beacons_ns)
    api.add_namespace(position_ns)
    api.add_namespace(players_ns)
    flask_app.register_blueprint(blueprint)
    db.init_app(flask_app)

def main():
    try:
        enable_ssl = sys.argv[1] == 'ssl'
    except IndexError:
        enable_ssl = False
    initialize_app(app)
    if enable_ssl:
        context = ('/home/mgalka/cert/domain.crt', '/home/mgalka/cert/domain.key')
        app.run(host='0.0.0.0', port=5000, ssl_context=context, threaded=True, debug=True)
    else:
        app.run(debug=True)

if __name__ == '__main__':
    main()