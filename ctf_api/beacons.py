from flask import request, jsonify
from flask_restplus import Resource, cors
from ctf_api.restplus import api
from ctf_api.data import beacons

ns = api.namespace('ctf/beacons', description='Beacon operations')


@ns.route('/')
class BeaconCollection(Resource):

    @cors.crossdomain(origin='*')
    def get(self):
        """
        Returns list of beacons.
        """
        return jsonify(beacons)


@ns.route('/<string:beacon_name>')
class BeaconItem(Resource):
    @cors.crossdomain(origin='*')
    def put(self, beacon_name):
        for beacon in beacons:
            if beacon_name.lower() == beacon['name'].lower():
                lat, lon = request.json['lat'], request.json['lon']
                beacon['lat'] = lat
                beacon['lon'] = lon
                beacon['owner'] = None
                return jsonify(beacons)
        return None, 404

